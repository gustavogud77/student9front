import { Component, OnInit } from '@angular/core';
import { Song } from 'src/app/models/song.model';

import { SongsService } from 'src/app/services/songs.service';

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.css']
})
export class SongsComponent implements OnInit {

  songs: Song[] = [];

  constructor(private songsService: SongsService) { }

  ngOnInit(): void {
    this.getAllSongs();
  }

  getAllSongs() {
    this.songsService.getAllSongs().subscribe(response => {
      this.songs = Object.assign([], response);
      console.log(response);
    });
  }
}
