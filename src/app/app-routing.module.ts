import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SongsComponent } from './components/songs/songs.component';

const routes: Routes = [
  { path: 'songs', pathMatch: 'full', component: SongsComponent },
  { path:'**', redirectTo: '/songs' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
