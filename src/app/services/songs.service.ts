import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { JSONObject } from '../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class SongsService {
  
  private apiUrl: string;
  private appUrl: string;

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
    this.appUrl = '/songs';
  }

  getAllSongs():Observable<JSONObject> {
    return this.http.get<JSONObject>(this.apiUrl + this.appUrl + '/all/0/0', this.httpOptions);
  }
}
