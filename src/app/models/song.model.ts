export interface Song {
    id?: number,
    artista?: string,
    name?: string,
    album?: string,
    released?: Date,
    playtime?: string,
    visible?: boolean,
    createdAt?: Date,
    updatedAt?: Date
}